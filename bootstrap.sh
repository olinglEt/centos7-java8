# yumの更新
sudo yum update

# vimのインストール
sudo yum -y install vim

# lsusbのインストール(LPIC用)
sudo yum -y install usbutils

# java開発環境のインストール
sudo yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel
