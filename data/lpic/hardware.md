## ハードウェア
### デバイス情報(ls /dev)
```shell
$ ls /dev
```

### CPU情報(cat /proc/cpuinfo)
```shell
$ cat /proc/cpuinfo
```

### PCIデバイスの情報(lspci)
```shell
$ lspci
$ lspci -v  # 詳細な情報
$ lspci -vv # より詳細な情報
```

### USBデバイスの情報(lsusb)
```shell
$ lsusb
$ lsusb -v    # 詳細な情報
$ lsusb -t    # ツリー状に表示
```

### デバイスドライバの情報(lsmod)
```shell
$ lsmod
```

### デバイスドライバの手動ロード(modprobe)
```shell
# modprobe e1000
```

### システム起動時のカーネルのイベント情報(dmesg)
```shell
$ dmesg
```

### シャットダウン(shutdown)
```shell
# shutdown -h now # 今すぐシャットダウン
# shutdown -r +5  # 5分後にシャットダウンして再起動
# shutdown -f # 次回起動時にfsckをスキップする
# shutdown -F # 次回起動時にfsckを強制する
# shutdown -k # シャットダウンせず警告を通知
# shutdown -c # シャットダウンをキャンセルする
```
### ランレベル(runlevel)
```shell
$ runlevel
```

### ランレベルの変更(init)
```shell
# init 1    # シングルユーザーモード(rootだけが使える)
```
