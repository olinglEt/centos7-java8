# centos7-java8
CentOS7にJava8の開発環境を構築した仮想マシン
## 事前インストール
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## VM起動
以下のコマンドを実行(初回は時間がかかります)
```shell
vagrant up
```

## VM終了
使い終わったら必ず終了しておく
```shell
vagrant halt
```

## ログイン
```shell
vagrant ssh
cd /share
```
ユーザー名とパスワードはどちらも`vagrant`と入力します
## 共有ディレクトリ
ゲスト側(CentOS)の`/share`とホスト側の`<Vagrantfileがあるディレクトリ>/data`が同期されています
## vimの設定
仮想マシンの`share`ディレクトリで以下を実行
```shell
sh ./vimconf.sh
```